#!/usr/bin/python3

import configparser
import logging
import sys
import unicodedata

from typing import Optional

import discord


def emoji_to_markdown(string: str) -> str:
    def mapper(item: str) -> str:
        if unicodedata.category(item) == "So":
            name = unicodedata.name(item)
            return f'`{name}`'
        else:
            return item
    return ''.join([i for i in map(mapper, string) if i != ""])


def emoji_to_text(string: str) -> str:
    return '+'.join(map(unicodedata.name, string.replace('\N{VARIATION SELECTOR-16}', '')
                                                .replace('\N{ZERO WIDTH JOINER}', '')))


def format_reaction_payload(payload: discord.RawReactionActionEvent) -> str:
    member: Optional[discord.Member] = payload.member
    membername: str = str(payload.user_id)

    if member:
        membername = member.nick or f"{member.name}#{member.discriminator}"

    emoji: str = "Oops"
    if not payload.emoji.name:
        emoji = "DELETED_CUSTOM_EMOJI"
    elif payload.emoji.is_unicode_emoji():
        emoji = emoji_to_text(payload.emoji.name)
    else:
        emoji = payload.emoji.name + '+' + str(payload.emoji.id)

    return f"{membername} performed {payload.event_type} {payload.message_id}@{payload.guild_id}: '{emoji}'"


class RoleBot(discord.Client):
    REQUIRED_CONFIG_ITEMS = {"RoleBot": ["token"]}

    def __init__(self, config: configparser.ConfigParser, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._config = config

        for sec, items in RoleBot.REQUIRED_CONFIG_ITEMS.items():
            if sec not in self._config.sections():
                raise configparser.NoSectionError(f"Required section '{sec}' not found in config file")
            for key in items:
                if key not in self._config[sec].keys():
                    raise configparser.NoOptionError(f"Required item '{key}' not found in section '{sec}'", sec)
        self._token = self._config["RoleBot"]["token"]

        self._logger = logging.getLogger("RoleBot")
        self._logger.setLevel(self._config.get("RoleBot", "loglevel", fallback="info").upper())

        formatter = logging.Formatter('{asctime} [{levelname:<8}][{name}] {message}', style='{')
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(formatter)
        self._logger.addHandler(stdout_handler)
        logfile = self._config.get("RoleBot", "logfile", fallback=None)
        if logfile:
            self._logger.debug(f"Opening logfile: {logfile}")
            file_handler = logging.FileHandler(filename=logfile, encoding='utf8', mode='a')
            file_handler.setFormatter(formatter)
            self._logger.addHandler(file_handler)

        self._bg = self.loop.create_task(self._set_status())

    async def on_connect(self):
        self._logger.info("Connected")

    async def on_ready(self):
        self._logger.info("Ready")

    async def on_disconnect(self):
        self._logger.warn("Disconnected")

    async def on_resume(self):
        self._logger.info("Reconnected")

    def run(self):
        self._logger.info("Running bot")
        super().run(self._token)
        self._logger.info("Shutting down")

    async def _set_status(self, activity: discord.BaseActivity = discord.Game("The Game"),
                          status: discord.Status = discord.Status.online):
        await self.wait_until_ready()
        await self.change_presence(activity=activity, status=status)
        self._logger.debug("Presence set")

    def _get_guild_section(self, guild_id: int) -> str:
        return f"RoleBot:{guild_id}"

    def _emoji_to_config_key(self, emoji: discord.PartialEmoji) -> str:
        prefix = "emoji_"
        if emoji.is_unicode_emoji():
            return prefix + emoji_to_text(emoji.name)
        elif emoji.is_custom_emoji():
            return f'{prefix}{emoji.id}'
        else:
            self._logger.error(f"Emoji '{emoji.name}' is somehow neither a unicode or custom emoji")
            return '#'

    def _get_role_from_emoji(self, guild_id: int, emoji: discord.PartialEmoji) -> Optional[int]:
        section: str = self._get_guild_section(guild_id)
        if section not in self._config.sections():
            self._logger.warn(f"Unconfigured guild: {guild_id}")
            self._logger.info(f"Config should be placed in a section named '{section}'")
            return None

        config_key = self._emoji_to_config_key(emoji)
        if (roleid := self._config.getint(section, config_key, fallback=-1)) != -1:
            return roleid
        else:
            self._logger.debug(f"Unknown emoji key '{config_key}', ignoring")
            return None

    def _get_guild_role_messageid(self, guild_id: int) -> Optional[int]:
        section: str = self._get_guild_section(guild_id)
        if section not in self._config.sections():
            self._logger.warn(f"Unconfigured guild: {guild_id}")
            self._logger.info(f"Config should be placed in a section named '{section}'")
            return None
        if (messageid := self._config.getint(section, "messageid", fallback=-1)) != -1:
            return messageid
        else:
            return None

    async def _do_role_action_raw(self, member: discord.Member, role: discord.Role, event_type: str):
        try:
            if event_type == "REACTION_ADD":
                await member.add_roles(role, reason="Added by RoleBot")
            elif event_type == "REACTION_REMOVE":
                await member.remove_roles(role, reason="Removed by RoleBot")
            else:
                self._logger.error("Unknown event type: {event_type}")
        except discord.Forbidden:
            self._logger.error("Forbidden: Could not add/remove role {roleid}@{payload.guild_id}")
        except discord.HTTPException:
            self._logger.error("HTTPException: Could not add/remove role {roleid}@{payload.guild_id}")

    async def _do_role_action(self, payload: discord.RawReactionActionEvent):
        messageid = self._get_guild_role_messageid(payload.guild_id)
        if not messageid or messageid != payload.message_id:
            self._logger.debug(f"Ignored reaction to {payload.message_id}@{payload.guild_id}. Expected msg={messageid}")
            return

        config_key: str = self._emoji_to_config_key(payload.emoji)
        roleid: Optional[int] = self._get_role_from_emoji(payload.guild_id, payload.emoji)
        if not roleid:
            self._logger.debug(f"Ignored reaction to {messageid}@{payload.guild_id}. No role for emoji {config_key}")
            return

        guild: Optional[discord.Guild] = self.get_guild(payload.guild_id)
        if not guild:
            self._logger.warn(f"Somehow recieved a message for illegal guild: {payload.guild_id}. Ignoring")
            return

        role: Optional[discord.Role] = guild.get_role(roleid)
        if not role:
            self._logger.warn(f"Role {roleid}@{payload.guild_id} could not be found for emoji {config_key}")
            return

        member: discord.Member
        if payload.event_type == "REACTION_ADD":
            member = payload.member
        elif payload.event_type == "REACTION_REMOVE":
            member = guild.get_member(payload.user_id)
            if not member:
                self._logger.warn(f"Somehow recieved a message for illegal user: {payload.user_id}. Ignoring")
                return

        await self._do_role_action_raw(member, role, payload.event_type)

    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        reaction = format_reaction_payload(payload)
        self._logger.debug(f"Add reaction: {reaction}")
        await self._do_role_action(payload)

    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        reaction = format_reaction_payload(payload)
        self._logger.debug(f"Del reaction: {reaction}")
        await self._do_role_action(payload)

    async def on_message(self, message: discord.Message):
        if message.author.id == self.user.id:
            return
        if message.content.startswith("rolebot emoji"):
            if not message.reference:
                await message.reply("You need to reply to something.", delete_after=20)
                return
            async with message.channel.typing():
                orig: discord.Message = await message.channel.fetch_message(message.reference.message_id)
                text = emoji_to_markdown(orig.content)
                await message.reply(text)
