import argparse
import configparser

import discord

from rolebot.core import RoleBot


def config_dict(conf: configparser.ConfigParser) -> dict[str, dict[str, str]]:
    return {key: dict(val) for key, val in dict(conf).items()}


def main():
    override = "Overrides the value from the config file."
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command", help="Commands")

    parser.add_argument("-c", "--config", default="rolebot.cfg", help="Set the config file location.")
    parser.add_argument("-l", "--loglevel", help=f"Set log level. {override}",
                        choices=["critical", "error", "warning", "info", "debug"])
    parser.add_argument("-t", "--token", help=f"Set the bot token. {override}")

    # run_parser =
    subparsers.add_parser("run", help="Run the bot.")

    dump_parser = subparsers.add_parser("dump", help="Dump internal bot info.")
    dump_parser.add_argument("dumptypes", action="append",
                             help="Information to dump. Repeat to dump additional information types.",
                             choices=["config", "effectiveconfig"])

    args = parser.parse_args()

    if args.command in ["run", "dump"]:
        config = configparser.ConfigParser()
        config.read(args.config)
        if args.command == "dump" and "config" in args.dumptypes:
            print(config_dict(config))

        if args.loglevel:
            config["RoleBot"]["loglevel"] = args.loglevel
        if args.token:
            config["RoleBot"]["token"] = args.token

        if args.command == "run":
            intents: discord.Intents = discord.Intents.default()
            intents.members = True
            intents.messages = True

            bot = RoleBot(config, intents=intents)
            print("Starting")
            bot.run()
        elif args.command == "dump" and "effectiveconfig" in args.dumptypes:
            print(config_dict(config))
