MODULE ?= rolebot

# Path to where the venv will be created
VENV_DIR ?= $(MODULE)-venv

# Shell to use when performing operations on the venv, must support -c
VENV_SHELL ?= bash

# The python executable to use
PYTHON ?= python3

RM ?= rm -f
FIND ?= find

venv: $(VENV_DIR)/pyvenv.cfg deps

$(VENV_DIR)/pyvenv.cfg:
	$(PYTHON) -m venv $(VENV_DIR) --prompt "$(notdir $(shell pwd))"

deps: requirements.txt | $(VENV_DIR)/pyvenv.cfg
	$(VENV_SHELL) -c 'source "$(VENV_DIR)/bin/activate" && pip install --upgrade pip && pip install -r requirements.txt'

devdeps: devrequirements.txt | deps $(VENV_DIR)/pyvenv.cfg
	$(VENV_SHELL) -c 'source "$(VENV_DIR)/bin/activate" && pip install -r devrequirements.txt'

check: devdeps
	$(VENV_SHELL) -c 'source "$(VENV_DIR)/bin/activate" && mypy -m $(MODULE) || exit 1'

clean:
	$(RM) -r "$(VENV_DIR)"
	$(FIND) . -name '__pycache__' -execdir $(RM) -r {} \;

all: venv

.PHONY: venv clean deps check
