from setuptools import setup

import rolebot

setup(
    name="rolebot",
    version=rolebot.__version__,
    packages=["rolebot"],
    install_requires=['discord<2'],
    entry_points={
            "console_scripts": ["rolebot=rolebot.cli:main"]
    }
)
